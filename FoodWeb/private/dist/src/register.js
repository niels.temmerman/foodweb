import { initializeApp } from 'firebase/app';
import { getDatabase, set, ref, onValue } from "firebase/database";
import { getAuth,updateUser , createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyA67A0GM-xxRPYIkcw0lcRCIXfGSRUFUXI",
    authDomain: "food-web-dd552.firebaseapp.com",
    projectId: "food-web-dd552",
    storageBucket: "food-web-dd552.appspot.com",
    messagingSenderId: "1026524148985",
    appId: "1:1026524148985:web:dc95da1d28ad25d5531e28",
    databaseURL: "https://food-web-dd552-default-rtdb.europe-west1.firebasedatabase.app/",
};
  
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const auth = getAuth();

const register = async (e) => {
  try {
    document.getElementById("loadICON").style.display = "block";
    const name = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const email = document.getElementById("email").value;
    if (name.length == 0) {
      alert("name cannot be empty");
    } else {
      const { user } = await createUserWithEmailAndPassword(auth, email, password)
      console.log(`User ${user.uid} created`)
      await updateProfile(user, {
        displayName: name
      });
      console.log("User profile updated")
      setCookie('user', user, '/');
      CreateUserFieldsRTDB(user);
      setTimeout(goBack, 1000);
    }
  } catch (error) {
    document.getElementById("errorMSG").innerText = error;
    document.getElementById("loadICON").style.display = "none";
  }
}

function CreateUserFieldsRTDB(user){
    /*const followRef = ref(db, "usersFollow/" + user.displayName + "/follows/" + Date.now());
    set(followRef,{
        user: user.displayName,
    });*/

    const dbRef = ref(db, "users/" + Date.now());
    set(dbRef,{
        username:  user.displayName,
        user: user.uid,
    });
}

let btn = document.getElementById("loginBtn");
btn.addEventListener("click", register, 5000);

function goBack(){
    window.location = './login.html';    
}

function setCookie(name, value, path, options = {}) {
  options = {
      path: path,
      ...options
  }; if (options.expires instanceof Date) {
      options.expires = options.expires.toUTCString();
  } let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value)
  for (let optionKey in options) {
      updatedCookie += "; " + optionKey
      let optionValue = options[optionKey]
      if (optionValue !== true) {
          updatedCookie += "=" + optionValue
      }
  }
  document.cookie = updatedCookie;
}