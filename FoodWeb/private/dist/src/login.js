import { initializeApp } from 'firebase/app';
import { getDatabase, ref, onValue } from "firebase/database";
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyA67A0GM-xxRPYIkcw0lcRCIXfGSRUFUXI",
    authDomain: "food-web-dd552.firebaseapp.com",
    projectId: "food-web-dd552",
    storageBucket: "food-web-dd552.appspot.com",
    messagingSenderId: "1026524148985",
    appId: "1:1026524148985:web:dc95da1d28ad25d5531e28",
    databaseURL: "https://food-web-dd552-default-rtdb.europe-west1.firebasedatabase.app/",
};
  
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const auth = getAuth(app);

let btn = document.getElementById("loginBtn");
btn.addEventListener("click",login, 5000);

async function login(){
  document.getElementById("loadICON").style.display = "block";
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;
  let loggedInUsername = null;
  let loggedUID = null;
  try {
    const creds = await signInWithEmailAndPassword(auth, email, password);
    loggedInUsername = creds.user.displayName;
    loggedUID = creds.user.uid;
  } catch (error) {
    document.getElementById("errorMSG").innerText = error;
    document.getElementById("loadICON").style.display = "none";
  }
  
  if(loggedInUsername != undefined){
    setCookie('user', loggedInUsername, '/');
    setCookie('UID', loggedUID, '/');
    setTimeout(GoHome, 1000);
  }
  else{
    //location.reload();
  }
}

function GoHome()
{
  document.location ="./index.html"
}

function setCookie(name, value, path, options = {}) {
  options = {
      path: path,
      ...options
  }; if (options.expires instanceof Date) {
      options.expires = options.expires.toUTCString();
  } let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value)
  for (let optionKey in options) {
      updatedCookie += "; " + optionKey
      let optionValue = options[optionKey]
      if (optionValue !== true) {
          updatedCookie += "=" + optionValue
      }
  }
  document.cookie = updatedCookie;
}