import { initializeApp } from 'firebase/app';
import { getDatabase, onChildAdded, ref, onValue, get, set, child } from "firebase/database";
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";
import { gsap } from 'gsap';

const firebaseConfig = {
    apiKey: "AIzaSyA67A0GM-xxRPYIkcw0lcRCIXfGSRUFUXI",
    authDomain: "food-web-dd552.firebaseapp.com",
    projectId: "food-web-dd552",
    storageBucket: "food-web-dd552.appspot.com",
    messagingSenderId: "1026524148985",
    appId: "1:1026524148985:web:dc95da1d28ad25d5531e28",
    databaseURL: "https://food-web-dd552-default-rtdb.europe-west1.firebasedatabase.app/",
};

const app = initializeApp(firebaseConfig);
const db = getDatabase();
let updateCommentRef;
let commentRef;

document.getElementById("commentForm").addEventListener("submit", SetComment);

let logout = document.getElementById("logoutBtn");
logout.addEventListener("click", Logout, 5000);

function Logout(){
    var cookies = document.cookie.split(";");
    console.log("test");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    location.reload();
}

const itemTemplate = 
`
<div id="ClickRecipe" class="card hover:shadow-lg bg-gray-200">
    <div class="lg:flex m-4 sm:flex-col">
        <img style="" id="IMGID" class="sm:h-1/6 max-w-md"/>
        <div id="DESCRIPTIONDIV" class="flex m-4" >
        <span>DESCRIPTIONmsg </span>
        </div>
    </div>
    <div class="m-4">
        <span class="font-bold"> NAMEOFREC </span>
        <span class="block text-sm"> By USER </span>
    </div>

    <div class="flex m-4">
    <div class="flex m-4">
        <span class="mr-2">MIN Min</span>
        <img class="h-6 w-6" src="./images/timer.png"> </img>
    </div>
    <div id="infoRec" class="lg:flex sm:flex-col">
        <div class="flex m-4">
            <span class="mr-2">KCAL Kcal</span>
            <img class="h-6 w-6" src="./images/kcal.png"> </img>
        </div>
        <div class="flex m-4">
            <span class="mr-2">CARBS Carbs</span>
            <img class="h-6 w-6" src="./images/carbs.png"> </img>
        </div>
        <div class="flex m-4">
            <span class="mr-2">PROTEIN Protein</span>
            <img class="h-6 w-6" src="./images/protein.png"> </img>
        </div>
        <div class="flex m-4">
            <span class="mr-2">FATS Fats</span>
            <img class="h-6 w-6" src="./images/fats.webp"> </img>
        </div>
        <div class="flex m-4">
            <span class="mr-2">RATIO</span>
            <img class="h-6" src="./images/likedislike.webp"> </img>
        </div>
    </div>
    </div>

    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Recipe",
      "name": "RICHNAME",
      "author": "RICHAUTHOR",
      "image": "RICHIMG",
      "description": "RICHDISC",
      "totalTime": "RICHTIME",
      "nutrition": {
        "@type": "NutritionInformation",
        "calories": "RICHKCAL",
        "carbs": "RICHCARBS",
        "protein":"RICHPROT",
        "fats":"RICHFATS",
      },
    }
    </script>
</div>
`;

function SetComment(e){
    e.preventDefault();

    let comment =  document.getElementById("comment").value;
    const dbRef = ref(db, "recipes/" + commentRef + "/comments/" + Date.now());
    document.getElementById("comment").value = "";
    set(dbRef,{
        user: getCookie('user'),
        Comment: comment,
    });
}

if(getCookie('user') != undefined){
    const username = getCookie('user');
    const login = document.getElementById('loginBtn');
    const signup = document.getElementById('signupBtn');
    login.textContent = username;
    signup.textContent = "";
    login.href = "#";
    document.getElementById("logoutBtn").style.display = "block";
}

async function LoadClickedRecipe(){
    let id = getCookie('recipeToView');
    id = id.replace("/latest", "");
    id = id.replace("/follow", "");
    let uid = id.split("/")[0];
    let recipe = id.split("/")[1];
    const recipeReference = ref(db, "recipes/" + uid);
    updateCommentRef = ref(db, "recipes/" + id);
    commentRef = id.split("/")[0] + "/" + id.split("/")[1];

    await get(recipeReference, recipe).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            let nameNoSpace = obj.Name;
            nameNoSpace = nameNoSpace.replace(/\s/g, '');
            if(nameNoSpace == recipe){
                let element = document.getElementById("recipeToLoad");
                let temp = itemTemplate;
                temp = temp.replace("IMGID", obj.UID + "/" + nameNoSpace + "/img");
                temp = temp.replace("NAMEOFREC", obj.Name);
                temp = temp.replace("USER", obj.User);
                temp = temp.replace("MIN", obj.Time);
                temp = temp.replace("KCAL", obj.Kcal);
                temp = temp.replace("CARBS", obj.Carbs);
                temp = temp.replace("PROTEIN", obj.Protein);
                temp = temp.replace("FATS", obj.Fats);
                temp = temp.replace("RATIO", obj.LikeRatio);
                temp = temp.replace("DESCRIPTIONmsg", obj.Description);
                temp = temp.replace("DESCRIPTIONDIV", obj.UID + "/" + nameNoSpace + "/description");

                temp = temp.replace("RICHNAME", nameNoSpace);
                temp = temp.replace("RICHAUTHOR", obj.User);
                temp = temp.replace("RICHTIME", "PT" + obj.Time + "M");
                temp = temp.replace("RICHKCAL", obj.Kcal);
                temp = temp.replace("RICHCARBS", obj.Carbs);
                temp = temp.replace("RICHPROT", obj.Protein);
                temp = temp.replace("RICHFATS", obj.Fats);
                temp = temp.replace("RICHDISC", obj.Description);
                temp = temp.replace("RICHIMG", obj.downloadURL);

                let div = document.createRange().createContextualFragment(temp);
                element.appendChild(div); 
                document.getElementById(obj.UID + "/" + nameNoSpace + "/img").src = obj.downloadURL;
            }
        });
    } else {
        console.log("No data available");
    }
    }).catch((error) => {
        console.error(error);
    });
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

LoadClickedRecipe();

onChildAdded(updateCommentRef, (data) => {
    updateComments(data);
});
  
function updateComments(data){
    console.log("UPDATE");
    const comments = data.val();
    const comment = "<li>" + comments.user + ":" + comments.Comment + "</li>";
    document.getElementById("comments").innerHTML += comment;
}

const loadTL = gsap.timeline({defaults: {ease: 'power2.out'}});

loadTL.to("#slider", {
    y: "-100%",
    duration: 1.2,
});