import { initializeApp } from 'firebase/app';
import { getDatabase, ref, onChildAdded, onValue, get, set, child } from "firebase/database";
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";
import { getStorage, uploadBytes, ref as sRef, getDownloadURL } from "firebase/storage";
import { gsap } from 'gsap';

const firebaseConfig = {
    apiKey: "AIzaSyA67A0GM-xxRPYIkcw0lcRCIXfGSRUFUXI",
    authDomain: "food-web-dd552.firebaseapp.com",
    projectId: "food-web-dd552",
    storageBucket: "food-web-dd552.appspot.com",
    messagingSenderId: "1026524148985",
    appId: "1:1026524148985:web:dc95da1d28ad25d5531e28",
    databaseURL: "https://food-web-dd552-default-rtdb.europe-west1.firebasedatabase.app/",
};

const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();


let createRecipeBtn = document.getElementById("CreateRecipeBtn");
createRecipeBtn.addEventListener("click", openForm, 5000);

let cancelCreate = document.getElementById("CancelCreate");
cancelCreate.addEventListener("click", closeForm, 5000);

let submitcreate = document.getElementById("SubmitCreate");
submitcreate.addEventListener("click", CreateCookbook, 5000);

let logout = document.getElementById("logoutBtn");
logout.addEventListener("click", Logout, 5000);

function Logout(){
    var cookies = document.cookie.split(";");
    console.log("test");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    location.reload();
}

function openForm() {
    document.getElementById("createForm").style.display = "block";
}
  
function closeForm() {
    document.getElementById("createForm").style.display = "none";
}


function CreateCookbook(){
    const recipeReferenceSet = ref(db, 'cookbooks/' + getCookie("UID") + "/" + document.getElementById("NameCook").value);
    const recipes = document.getElementById("RecCook").value.split("_");
    set(recipeReferenceSet, {
        Name: document.getElementById("NameCook").value,
        User: getCookie('user'),
        UID: getCookie('UID'),
        Description: document.getElementById("DescCook").value,
        Recipes: recipes
    });

    setTimeout(function(){
        location.reload();
    }, 1000);
}

closeForm();

const itemTemplate = 
`
<div id="ClickRecipe" class="card hover:shadow-lg bg-gray-200 m-4 max-h-xs">
    <div class="lg:flex m-4">
        <img style="max-width:600px; width:100%" id=IMGID class=""/>
        <div id="DESCRIPTIONDIV" style="display:none" class="flex m-4" >
            <span>DESCRIPTIONmsg </span>
        </div>
    </div>
    <div class="m-4">
        <span class="font-bold"> NAMEOFREC </span>
        <span class="block text-sm"> By USER </span>
    </div>
    
    <div class="flex m-4 flex-wrap">
        <div class="flex m-4">
            <img class="h-6 w-6" src="./images/timer.png"> </img>
            <span>MIN Min</span>
        </div>
        <div id="infoRec" class="flex-wrap" style="display:none">
            <div class="flex m-4">
                <img class="h-6 w-6" src="./images/kcal.png"> </img>
                <span>KCAL Kcal</span>
            </div>
            <div class="flex m-4">
                <img class="h-6 w-6" src="./images/carbs.png"> </img>
                <span>CARBS Carbs</span>
            </div>
            <div class="flex m-4">
                <img class="h-6 w-6" src="./images/protein.png"> </img>
                <span>PROTEIN Protein</span>
            </div>
            <div class="flex m-4">
                <img class="h-6 w-6" src="./images/fats.webp"> </img>
                <span>FATS Fats</span>
            </div>
        </div>
    </div>

    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Recipe",
      "name": "RICHNAME",
      "author": "RICHAUTHOR",
      "image": "RICHIMG",
      "description": "RICHDISC",
      "totalTime": "RICHTIME",
      "nutrition": {
        "@type": "NutritionInformation",
        "calories": "RICHKCAL",
        "carbs": "RICHCARBS",
        "protein":"RICHPROT",
        "fats":"RICHFATS"
      }
    }
    </script>

</div>
`;

const cookBookTemplate = 
`
<div id="ClickBook" class="card hover:shadow-lg bg-gray-200 cursor-pointer m-4  max-h-xs flex-col">
    <div class="m-4 flex-row">
        <span class="font-bold"> NAMEOFBOOK </span>
        <span class="block text-sm"> By USER </span>
        <div id="DESCRIPTIONDIV" class="flex m-4" >
            <span>DESCRIPTIONmsg </span>
        </div>
    </div>
    <h1 class="font-semibold ml-4"> Recipes: </h1>
    <div id="CookRecDiv" class="flex-row">
        
    </div>
</div>
`;


function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

if(getCookie('user') != undefined){
    const username = getCookie('user');
    const login = document.getElementById('loginBtn');
    const signup = document.getElementById('signupBtn');
    login.textContent = username;
    signup.style.display = "none";
    login.href = "";
    document.getElementById('cookbookATag').style.display = "inline";
    document.getElementById("logoutBtn").style.display = "block";
    try {
        LoadMyRecipes(); 
        LoadMyCookbooks();
    } catch (error) {
        console.log(error);
    }
}

function LoadMyRecipes(){
    const UID = getCookie('UID');
    const recipeReference = ref(db, 'recipes/');
    get(child(recipeReference, UID)).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            let nameNoSpace = obj.Name;
            nameNoSpace = nameNoSpace.replace(/\s/g, '');
            let element = document.getElementById("myRecDiv");

            let temp = itemTemplate;
            temp = temp.replace("IMGID", obj.UID + "/" + nameNoSpace + "/img");
            temp = temp.replace("NAMEOFREC", obj.Name);
            temp = temp.replace("USER", obj.User);
            temp = temp.replace("MIN", obj.Time);
            temp = temp.replace("KCAL", obj.Kcal);
            temp = temp.replace("CARBS", obj.Carbs);
            temp = temp.replace("PROTEIN", obj.Protein);
            temp = temp.replace("FATS", obj.Fats);
            temp = temp.replace("IDBTN1", obj.UID + "/" + nameNoSpace);
            temp = temp.replace("IDBTN2", obj.UID + "/" + nameNoSpace + "/dis");
            temp = temp.replace("IDBTN3", obj.UID + "/" + nameNoSpace + "/comments");
            temp = temp.replace("DESCRIPTIONmsg", obj.Description);
            temp = temp.replace("DESCRIPTIONDIV", obj.UID + "/" + nameNoSpace + "/description");

            temp = temp.replace("ClickRecipe", obj.UID + "/" + nameNoSpace + "REC");
            temp = temp.replace("infoRec", obj.UID + "/" + nameNoSpace + "INFO");

            temp = temp.replace("RICHNAME", nameNoSpace);
            temp = temp.replace("RICHAUTHOR", obj.User);
            temp = temp.replace("RICHTIME", "PT" + obj.Time + "M");
            temp = temp.replace("RICHKCAL", obj.Kcal);
            temp = temp.replace("RICHCARBS", obj.Carbs);
            temp = temp.replace("RICHPROT", obj.Protein);
            temp = temp.replace("RICHFATS", obj.Fats);
            temp = temp.replace("RICHDISC", obj.Description);
            temp = temp.replace("RICHIMG", obj.downloadURL);

            let div = document.createRange().createContextualFragment(temp);
            element.appendChild(div);
            document.getElementById(obj.UID + "/" + nameNoSpace + "/img").src = obj.downloadURL;
        });
    } else {
        console.log("No data available");
    }
    }).catch((error) => {
        console.error(error);
    });
}

function LoadMyCookbooks(){
    const UID = getCookie('UID');
    const cookbookReference = ref(db, 'cookbooks/');
    
    get(child(cookbookReference, UID)).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            let bookID = obj.UID + "/" + obj.Name + "/div";
            bookID = bookID.replace(/\s/g, '');
            let element = document.getElementById("myCookbookDiv");
            let recipes = obj.Recipes;
            let temp = cookBookTemplate;
            temp = temp.replace("NAMEOFBOOK", obj.Name);
            temp = temp.replace("USER", obj.User);
            temp = temp.replace("DESCRIPTIONmsg", obj.Description);
            temp = temp.replace("DESCRIPTIONDIV", obj.UID + "/" + obj.Name + "/description");
            temp = temp.replace("CookRecDiv", bookID);

            let div = document.createRange().createContextualFragment(temp);
            element.appendChild(div);
            

            recipes.forEach(rec => {
                LoadCookRec(rec, bookID);
            });
        });
    } else {
        console.log("No data available");
    }
    }).catch((error) => {
        console.error(error);
    });
}

function LoadCookRec(rec, bookID){
    const UID = getCookie('UID');
    const recipeReference = ref(db, 'recipes/');
    get(child(recipeReference, UID)).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            let nameNoSpace = obj.Name;
            nameNoSpace = nameNoSpace.replace(/\s/g, '');
            let element = document.getElementById(bookID);
            let temp = itemTemplate;

            if(obj.Name == rec){
                temp = temp.replace("IMGID", bookID + "/" + nameNoSpace + "/img");
                temp = temp.replace("NAMEOFREC", obj.Name);
                temp = temp.replace("USER", obj.User);
                temp = temp.replace("MIN", obj.Time);
                temp = temp.replace("KCAL", obj.Kcal);
                temp = temp.replace("CARBS", obj.Carbs);
                temp = temp.replace("PROTEIN", obj.Protein);
                temp = temp.replace("FATS", obj.Fats);
                temp = temp.replace("DESCRIPTIONmsg", obj.Description);
                temp = temp.replace("DESCRIPTIONDIV", obj.UID + "/" + nameNoSpace + "/description");
                temp = temp.replace("ClickRecipe", obj.UID + "/" + nameNoSpace + "REC");
                temp = temp.replace("infoRec", obj.UID + "/" + nameNoSpace + "INFO");
                temp = temp.replace("RICHNAME", nameNoSpace);
                temp = temp.replace("RICHAUTHOR", obj.User);
                temp = temp.replace("RICHTIME", "PT" + obj.Time + "M");
                temp = temp.replace("RICHKCAL", obj.Kcal);
                temp = temp.replace("RICHCARBS", obj.Carbs);
                temp = temp.replace("RICHPROT", obj.Protein);
                temp = temp.replace("RICHFATS", obj.Fats);
                temp = temp.replace("RICHDISC", obj.Description);
                temp = temp.replace("RICHIMG", obj.downloadURL);

                let div = document.createRange().createContextualFragment(temp);
                element.appendChild(div);
                console.log(bookID + "/" + nameNoSpace + "/img")
                document.getElementById(bookID + "/" + nameNoSpace + "/img").src = obj.downloadURL;
                document.getElementById(obj.UID + "/" + nameNoSpace + "INFO").style.display = 'flex';
            }
        });
    }
    });
}