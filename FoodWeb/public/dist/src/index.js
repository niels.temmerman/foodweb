import { initializeApp } from 'firebase/app';
import { getDatabase, ref, onChildAdded, onValue, get, set, child } from "firebase/database";
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";
import { getStorage, uploadBytes, ref as sRef, getDownloadURL } from "firebase/storage";
import { gsap } from 'gsap';

const firebaseConfig = {
    apiKey: "AIzaSyA67A0GM-xxRPYIkcw0lcRCIXfGSRUFUXI",
    authDomain: "food-web-dd552.firebaseapp.com",
    projectId: "food-web-dd552",
    storageBucket: "food-web-dd552.appspot.com",
    messagingSenderId: "1026524148985",
    appId: "1:1026524148985:web:dc95da1d28ad25d5531e28",
    databaseURL: "https://food-web-dd552-default-rtdb.europe-west1.firebasedatabase.app/",
};


let buttons = null;

const buttonPressed = e => {
    const id = e.target.id;
    if(id.indexOf('/') > -1){
        if(id.indexOf('dis') > -1){
            DisLikeRecipe(id);
        }
        else if(id.indexOf('comment') > -1){
            GoToRecipeView(id);
        }
        else{
            LikeRecipe(id);
        }
    }
}

const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();
let downloadURL;

let updateFollowRef;
let followedToLoad = [];

let popularRecRef;

let createRecipeBtn = document.getElementById("CreateRecipeBtn");
createRecipeBtn.addEventListener("click", openForm, 5000);

let cancelCreate = document.getElementById("CancelCreate");
cancelCreate.addEventListener("click", closeForm, 5000);

let submitCreate = document.getElementById("SubmitCreate");
submitCreate.addEventListener("click", CreateRecipe, 5000);

let follow = document.getElementById("followBtn");
follow.addEventListener("click", FollowUser, 5000);

let logout = document.getElementById("logoutBtn");
logout.addEventListener("click", Logout, 5000);

function Logout(){
    var cookies = document.cookie.split(";");
    console.log("test");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    location.reload();
}

function GoToRecipeView(id){
    setCookie('recipeToView', id, '/');
    window.location.href = "recipe.html";
}

function CreateRecipe(){
    UploadPhoto();
}

function SetRecipe(url){
    let nameNoSpace = document.getElementById("NameRec").value;
    nameNoSpace = nameNoSpace.replace(/\s/g, '');
    const recipeReferenceSet = ref(db, 'recipes/' + getCookie("UID") + "/" + nameNoSpace);
    
    set(recipeReferenceSet, {
        Name: document.getElementById("NameRec").value,
        Time: document.getElementById("TimeRec").value,
        Kcal: document.getElementById("KcalRec").value,
        Carbs: document.getElementById("CarbsRec").value,
        Protein: document.getElementById("ProteinRec").value,
        Fats: document.getElementById("FatsRec").value,
        User: getCookie('user'),
        UID: getCookie('UID'),
        Description: document.getElementById("DescRec").value,
        LikeRatio: 0,
        downloadURL: url,
    });

    setTimeout(function(){
        location.reload();
    }, 1000);
}

function UploadPhoto(){
    let downloadURL;
    const file = document.getElementById("imgupload").files[0];
    const name = new Date() + "-" + file.name;
    const imgref = sRef(storage, 'images/' + name);

    const metadata = {
    contentType: file.type,
    };

    const uploadTask = uploadBytes(imgref, file, metadata)
    .then((snapshot) => {
    getDownloadURL(snapshot.ref).then((url) => {
        SetRecipe(url);
    });
    }).catch((error) => {
    console.error('Upload failed', error);
    });
}

function openForm() {
    document.getElementById("createForm").style.display = "block";
}
  
function closeForm() {
    document.getElementById("createForm").style.display = "none";
}

function removeCreateBtn(){
    document.getElementById("CreateRecipeBtn").style.display = "none";
}

closeForm();
removeCreateBtn();

const itemTemplate = 
`
<div id="ClickRecipe" class="card hover:shadow-lg bg-gray-200 mt-8  max-h-xs">
    <div class="lg:flex m-4">
        <img style="max-width:400px;" id=IMGID class=""/>
        <div id="DESCRIPTIONDIV" style="display:none" class="flex m-4" >
            <span>DESCRIPTIONmsg </span>
        </div>
    </div>
    <div class="m-4">
        <span class="font-bold"> NAMEOFREC </span>
        <span class="block text-sm"> By USER </span>
    </div>
    
    <div class="flex m-4">
        <div class="flex m-4">
            <span class="mr-2">MIN Min</span>
            <img class="h-6 w-6" src="./images/timer.png"> </img>
        </div>
        <div id="infoRec" class="flex-wrap" style="display:none">
            <div class="flex m-4">
                <span class="mr-2">KCAL Kcal</span>
                <img class="h-6 w-6" src="./images/kcal.png"> </img>
            </div>
            <div class="flex m-4">
                <span class="mr-2">CARBS Carbs</span>
                <img class="h-6 w-6" src="./images/carbs.png"> </img>
            </div>
            <div class="flex m-4">
                <span class="mr-2">PROTEIN Protein</span>
                <img class="h-6 w-6" src="./images/protein.png"> </img>
            </div>
            <div class="flex m-4">
                <span class="mr-2">FATS Fats</span>
                <img class="h-6 w-6" src="./images/fats.webp"> </img>
            </div>
        </div>
    </div>
    <div class="float-right">
        <button id="IDBTN3" type="button" class="bg-main3 hover:bg-main4 focus:outline-none focus:ring-4 font-medium rounded-full text-sm px-6 py-3 text-center mr-2 mb-2 bg-comment bg-contain bg-center bg-no-repeat">
        </button>
        <button id="IDBTN1" type="button" class="bg-main3 hover:bg-main4 focus:outline-none focus:ring-4 font-medium rounded-full text-sm px-6 py-3 text-center mr-2 mb-2 bg-like bg-contain bg-center bg-no-repeat">
        </button>
        <button id="IDBTN2" type="button" class="bg-main3 hover:bg-main4 focus:outline-none focus:ring-4 font-medium rounded-full text-sm px-6 py-3 text-center mr-2 mb-2 bg-dislike bg-contain bg-center bg-no-repeat">
        </button>
    </div>

    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Recipe",
      "name": "RICHNAME",
      "author": "RICHAUTHOR",
      "image": "RICHIMG",
      "description": "RICHDISC",
      "totalTime": "RICHTIME",
      "nutrition": {
        "@type": "NutritionInformation",
        "calories": "RICHKCAL",
        "carbs": "RICHCARBS",
        "protein":"RICHPROT",
        "fats":"RICHFATS"
      }
    }
    </script>

</div>
`;

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

if(getCookie('user') != undefined){
    const username = getCookie('user');
    const login = document.getElementById('loginBtn');
    const signup = document.getElementById('signupBtn');
    login.textContent = username;
    signup.style.display = "none";
    login.href = "";
    document.getElementById('cookbookATag').style.display = "inline";
    document.getElementById("CreateRecipeBtn").style.display = "block";
    document.getElementById("searchBAR").style.display = "block";
    document.getElementById("logoutBtn").style.display = "block";
    try {
        LoadMyRecipes();
        LoadFollowRecipes();
        
    } catch (error) {
        console.log(error);
    }
}

function LoadAnimations(ObjID){
    const rec =  document.getElementById(ObjID + "REC");
    const info = document.getElementById(ObjID + "INFO");
    const likeBtn = document.getElementById(ObjID);
    const dislikeBtn = document.getElementById(ObjID + "/dis");
    const commentBtn = document.getElementById(ObjID + "/comments");
    const description = document.getElementById(ObjID + "/description");
    const image = document.getElementById(ObjID + "/img");

    let infoTL = gsap.timeline({ paused: true});
    let divTL = gsap.timeline({ paused: true});
    let cBtn = gsap.timeline({ paused: true});
    let lBtn = gsap.timeline({ paused: true});
    let dlBtn = gsap.timeline({ paused: true});
    let desc = gsap.timeline({ paused: true});
    let img = gsap.timeline({ paused: true});

    img.to(image, {
        maxWidth: '600px'
    });

    desc.to(description, {
        display: 'flex',
        delay: 1,
        duration: 0,
    });

    cBtn.to(commentBtn, {
        width: 85,
        height: 60,
        delay: 0,
        duration: 1,
    });

    lBtn.to(likeBtn, {
        width: 85,
        height: 60,
        delay: 0,
        duration: 1,
    });

    dlBtn.to(dislikeBtn, {
        width: 85,
        height: 60,
        delay: 0,
        duration: 1,
    });

    infoTL.to(info, {
        display: 'flex',
        delay: 1,
        duration: 0,
    });
    
    divTL.to(rec, {
        width: window.innerWidth / 3 * 2,
        delay: 0,
        duration: 1,
    });

    rec.addEventListener("mouseenter", (e) => {
        infoTL.play();
        desc.play();
        if(window.innerWidth > 1000){
            divTL.play();
            cBtn.play();
            lBtn.play();
            dlBtn.play();
            img.play();
        }
    });
    rec.addEventListener("mouseleave", (e) => {
        infoTL.reverse();
        desc.reverse();
        if(window.innerWidth > 1000){
            divTL.reverse();
            cBtn.reverse();
            lBtn.reverse();
            dlBtn.reverse();
            img.reverse();
        }
    });
}

function LoadMyRecipes(){
    const UID = getCookie('UID');
    const recipeReference = ref(db, 'recipes/');
    get(child(recipeReference, UID)).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            
            let element = document.getElementById("myRecDiv");

            let temp = itemTemplate;

            let nameNoSpace = obj.Name;
            nameNoSpace = nameNoSpace.replace(/\s/g, '');

            temp = temp.replace("IMGID", obj.UID + "/" + nameNoSpace + "/img");
            temp = temp.replace("NAMEOFREC", obj.Name);
            temp = temp.replace("USER", obj.User);
            temp = temp.replace("MIN", obj.Time);
            temp = temp.replace("KCAL", obj.Kcal);
            temp = temp.replace("CARBS", obj.Carbs);
            temp = temp.replace("PROTEIN", obj.Protein);
            temp = temp.replace("FATS", obj.Fats);
            temp = temp.replace("IDBTN1", obj.UID + "/" + nameNoSpace);
            temp = temp.replace("IDBTN2", obj.UID + "/" + nameNoSpace + "/dis");
            temp = temp.replace("IDBTN3", obj.UID + "/" + nameNoSpace + "/comments");
            temp = temp.replace("DESCRIPTIONmsg", obj.Description);
            temp = temp.replace("DESCRIPTIONDIV", obj.UID + "/" + nameNoSpace + "/description");

            temp = temp.replace("ClickRecipe", obj.UID + "/" + nameNoSpace + "REC");
            temp = temp.replace("infoRec", obj.UID + "/" + nameNoSpace + "INFO");

            temp = temp.replace("RICHNAME", nameNoSpace);
            temp = temp.replace("RICHAUTHOR", obj.User);
            temp = temp.replace("RICHTIME", "PT" + obj.Time + "M");
            temp = temp.replace("RICHKCAL", obj.Kcal);
            temp = temp.replace("RICHCARBS", obj.Carbs);
            temp = temp.replace("RICHPROT", obj.Protein);
            temp = temp.replace("RICHFATS", obj.Fats);
            temp = temp.replace("RICHDISC", obj.Description);
            temp = temp.replace("RICHIMG", obj.downloadURL);

            let div = document.createRange().createContextualFragment(temp);
            element.appendChild(div);
            document.getElementById(obj.UID + "/" + nameNoSpace + "/img").src = obj.downloadURL;
            LoadAnimations(obj.UID + "/" + nameNoSpace);
            InitializeButton();
        });
    } else {
        console.log("No data available");
    }
    }).catch((error) => {
        console.error(error);
    });
}

function LoadPopularRecipes(){
    popularRecRef = ref(db,);

    get(child(popularRecRef, "recipes/")).then((snapshot) => {
        if (snapshot.exists()) {
            snapshot.forEach(function (snapshot) {
                let UID = snapshot.key;
                get(child(popularRecRef, "recipes/" + UID + "/")).then((snapshot) => {
                    if (snapshot.exists()) {
                        let element;
                        let temp;
                        let obj
                        snapshot.forEach(function (snapshot) {
                            obj = snapshot.val();
                            let nameNoSpace = obj.Name;
                            nameNoSpace = nameNoSpace.replace(/\s/g, '');
                            
                            element = document.getElementById("latestRecDiv");
                            temp = itemTemplate;
                            temp = temp.replace("IMGID", obj.UID + "/" + nameNoSpace + "/latest" + "/img");
                            temp = temp.replace("NAMEOFREC", nameNoSpace);
                            temp = temp.replace("USER", obj.User);
                            temp = temp.replace("MIN", obj.Time);
                            temp = temp.replace("KCAL", obj.Kcal);
                            temp = temp.replace("CARBS", obj.Carbs);
                            temp = temp.replace("PROTEIN", obj.Protein);
                            temp = temp.replace("FATS", obj.Fats);
                            temp = temp.replace("IDBTN1", obj.UID + "/" + nameNoSpace + "/latest");
                            temp = temp.replace("IDBTN2", obj.UID + "/" + nameNoSpace + "/latest" + "/dis");
                            temp = temp.replace("IDBTN3", obj.UID + "/" + nameNoSpace + "/latest" + "/comments"); 
                            temp = temp.replace("DESCRIPTIONmsg", obj.Description);
                            temp = temp.replace("DESCRIPTIONDIV", obj.UID + "/" + nameNoSpace  + "/latest" + "/description");

                            temp = temp.replace("ClickRecipe", obj.UID + "/" + nameNoSpace + "/latest" + "REC" );
                            temp = temp.replace("infoRec", obj.UID + "/" + nameNoSpace + "/latest" + "INFO");

                            temp = temp.replace("RICHNAME", nameNoSpace);
                            temp = temp.replace("RICHAUTHOR", obj.User);
                            temp = temp.replace("RICHTIME", "PT" + obj.Time + "M");
                            temp = temp.replace("RICHKCAL", obj.Kcal);
                            temp = temp.replace("RICHCARBS", obj.Carbs);
                            temp = temp.replace("RICHPROT", obj.Protein);
                            temp = temp.replace("RICHFATS", obj.Fats);
                            temp = temp.replace("RICHDISC", obj.Description);
                            temp = temp.replace("RICHIMG", obj.downloadURL);
                            
                            if(obj.LikeRatio > 10){
                                let div = document.createRange().createContextualFragment(temp);
                                element.appendChild(div);
                                document.getElementById(obj.UID + "/" + nameNoSpace + "/latest" + "/img").src = obj.downloadURL;
                                LoadAnimations(obj.UID + "/" + nameNoSpace + "/latest");
                                InitializeButton();
                            }
                        });
                        
                    }
                })
            });
        } 
    }).catch((error) => {
        console.error(error);
    });
}

LoadPopularRecipes();

function LoadFollowRecipes(){
    const user = getCookie('user');
    updateFollowRef = ref(db, 'usersFollow/' + user + "/follows/");
}

async function LikeRecipe(id){
    let comments = 0;
    let DownloadURL = 0;
    let desc = 0;
    let carbs = 0; 
    let fats = 0; 
    let kcal = 0; 
    let name = 0;
    let protein = 0; 
    let time = 0; 
    let user = 0;
    let uid = id.split("/")[0];
    let recipe = id.split("/")[1];
    const recipeReference = ref(db, "recipes/" + uid);
    let currentLikeRatio = 0;
    await get(recipeReference, recipe).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            let nameNoSpace = obj.Name;
            nameNoSpace = nameNoSpace.replace(/\s/g, '');
            if(nameNoSpace == recipe){
                currentLikeRatio = obj.LikeRatio;
                carbs = obj.Carbs;
                fats = obj.Fats; 
                kcal = obj.Kcal; 
                name = obj.Name;
                protein = obj.Protein; 
                time = obj.Time; 
                user = obj.User;
                DownloadURL = obj.downloadURL;
                desc = obj.Description;
                comments = obj.comments;
            }
        });
    } else {
        console.log("No data available");
    }
    }).catch((error) => {
        console.error(error);
    });
    currentLikeRatio = currentLikeRatio + 1;
    SetLikeRatio(currentLikeRatio, id, carbs, fats, kcal, name, protein, time, user, DownloadURL, desc, comments);
}

async function DisLikeRecipe(id){
    let comments = 0;
    let DownloadURL = 0;
    let desc = 0;
    let carbs = 0; 
    let fats = 0; 
    let kcal = 0; 
    let name = 0;
    let protein = 0; 
    let time = 0; 
    let user = 0;
    let uid = id.split("/")[0];
    let recipe = id.split("/")[1];
    const recipeReference = ref(db, "recipes/" + uid);
    let currentLikeRatio = 0;
    await get(recipeReference, recipe).then((snapshot) => {
    if (snapshot.exists()) {
        snapshot.forEach(function (snapshot) {
            let obj = snapshot.val();
            let nameNoSpace = obj.Name;
            nameNoSpace = nameNoSpace.replace(/\s/g, '');
            if(nameNoSpace == recipe){
                currentLikeRatio = obj.LikeRatio;
                carbs = obj.Carbs;
                fats = obj.Fats; 
                kcal = obj.Kcal; 
                name = obj.Name;
                protein = obj.Protein; 
                time = obj.Time; 
                user = obj.User;
                DownloadURL = obj.downloadURL;
                desc = obj.Description;
                comments = obj.comments;
            }
        });
    } else {
        console.log("No data available");
    }
    }).catch((error) => {
        console.error(error);
    });
    currentLikeRatio = currentLikeRatio - 1;
    SetLikeRatio(currentLikeRatio, id, carbs, fats, kcal, name, protein, time, user, DownloadURL, desc, comments);
}

function SetLikeRatio(currentLikeRatio, id, carbs, fats, kcal, name, protein, time, user, DownloadURL, desc, comments){
    if (comments == null){
        comments = 0;
    }
    console.log("SET LIKE RATIO");
    console.log(comments);
    let uid = id.split("/")[0];
    let recipe = id.split("/")[1];
    const dbRef = ref(db, "recipes/" + uid + "/" + recipe);
    set(dbRef,{
        Name: name,
        Time: time,
        Kcal: kcal,
        Carbs: carbs,
        Protein: protein,
        Fats: fats,
        User: user,
        UID: uid,
        LikeRatio: currentLikeRatio,
        downloadURL: DownloadURL,
        Description: desc,
        comments: comments
    });
}

function InitializeButton(){
    buttons = document.getElementsByTagName("button");
    for (let button of buttons) {
        button.addEventListener("click", buttonPressed);
    }
}

function setCookie(name, value, path, options = {}) {
    options = {
        path: path,
        ...options
    }; if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    } let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value)
    for (let optionKey in options) {
        updatedCookie += "; " + optionKey
        let optionValue = options[optionKey]
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue
        }
    }
    document.cookie = updatedCookie;
}

function FollowUser(){
    let toFollow = document.getElementById("ToFollow").value;
    const dbRef = ref(db, "usersFollow/" + getCookie('user') + "/follows/" + Date.now());
    set(dbRef,{
        user: toFollow,
    });
}

function UpdateFollowRecipes(recUser){
    const refToRec =  ref(db, "recipes/");
    let UID;
    (async () => {
        UID = await GetUserUID(recUser)
    })()
    setTimeout(function(){
        get(child(refToRec, UID)).then((snapshot) => {
            if (snapshot.exists()) {
                let element;
                let temp;
                let obj;
                snapshot.forEach(function (snapshot) {
                    obj = snapshot.val();
                    let nameNoSpace = obj.Name;
                    nameNoSpace = nameNoSpace.replace(/\s/g, '');

                    element = document.getElementById("followRecDiv");
                    temp = itemTemplate;
                    temp = temp.replace("IMGID", obj.UID + "/" + nameNoSpace + "/img" + "/follow");
                    temp = temp.replace("NAMEOFREC", obj.Name);
                    temp = temp.replace("USER", obj.User);
                    temp = temp.replace("MIN", obj.Time);
                    temp = temp.replace("KCAL", obj.Kcal);
                    temp = temp.replace("CARBS", obj.Carbs);
                    temp = temp.replace("PROTEIN", obj.Protein);
                    temp = temp.replace("FATS", obj.Fats);
                    temp = temp.replace("IDBTN1", obj.UID + "/" + nameNoSpace + "/follow");
                    temp = temp.replace("IDBTN2", obj.UID + "/" + nameNoSpace + "/follow" + "/dis");
                    temp = temp.replace("IDBTN3", obj.UID + "/" + nameNoSpace + "/follow" + "/comments"); 

                    temp = temp.replace("RICHNAME", nameNoSpace);
                    temp = temp.replace("RICHAUTHOR", obj.User);
                    temp = temp.replace("RICHTIME", "PT" + obj.Time + "M");
                    temp = temp.replace("RICHKCAL", obj.Kcal);
                    temp = temp.replace("RICHCARBS", obj.Carbs);
                    temp = temp.replace("RICHPROT", obj.Protein);
                    temp = temp.replace("RICHFATS", obj.Fats);
                    temp = temp.replace("RICHDISC", obj.Description);
                    temp = temp.replace("RICHIMG", obj.downloadURL);

                    temp = temp.replace("max-width:400px;", "max-width:600px; width:100%");
                
                    let div = document.createRange().createContextualFragment(temp);
                    element.appendChild(div);
                    document.getElementById(obj.UID + "/" + nameNoSpace + "/img" + "/follow").src = obj.downloadURL;
                    InitializeButton();
                });
            } else {
                console.log("No data available");
            }
        }).catch((error) => {
            console.error(error);
        });
    }, 1000);
}

if(updateFollowRef != undefined){
    onChildAdded(updateFollowRef, (data) => {
        updateFolRec(data);
        console.log(data);
    });
}
  
function updateFolRec(data){
    const userToFollow = data.val().user;
    //followedToLoad.push(userToFollow);
    UpdateFollowRecipes(userToFollow);
}

async function GetUserUID(username){
    let UID;
    const userRef = ref(db, "users/")
    await get(userRef, username).then((snapshot) => {
        if (snapshot.exists()) {
            snapshot.forEach(function (snapshot) {
                let obj = snapshot.val();
                if(username == obj.username){
                    UID = obj.user;
                }
            });
        } else {
            console.log("No data available");
        }
        }).catch((error) => {
            console.error(error);
        });
    return UID;
}