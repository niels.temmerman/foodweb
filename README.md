# Web Topics Project Food Web


## Introduction

This is my web topics project called Food Web. On this website it is possible to create recipes, turn these recipes into a cookbook.
You can follow people, like posts, post comments, etc. And thus see each other's recipes.

## Website
The website consists of 6 pages.
* Home: All recipes are displayed here under different categories.
* Discover: An RSS Feed with additional recipes is displayed here.
* My Cookbooks: This page is only accessible when you are logged in. Here you can create cookbooks.
* About: Here you can find extra information about the website (currently template text).
* Login: Here you can log in.
* Register: Here you can register.

## Technologies used

* Firebase (Real Time Database, Hosting, Authentication)
* Tailwind
* RSS feed
* GSAP
* Structured Data
* Webpack

This website makes extensive use of Firebase.
Namely the Real Time Database, Hosting and Authentication.
To store all data, host the website and create users.
Web animations were also created with GSAP. These animations make recipes fold open when hovering over them and add a page transition animation to the recipe page.  
These animations are on in the index.js and recipe.js file.
Google rich results with structured data has been added to this website. The template is visible in the javascript files.
An RSS Feed is also present on the Discover page.
With webpack, the js files are bundled. So if you modify anything you will have to re-create the bundles.
The website is fully formatted with Tailwind.

## Use
* Use Live-server or similar (Visual Studio Code).
* Use your own firebase config (.js files). The config in the code expires in a week from this commit.
* And you can test. 

## File structure

* Private: This folder is used for local testing.
* Public: Firebase Hosting gets its files from this folder. When everything is finished, the contents of these folders will be the same.
